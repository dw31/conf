#!/bin/bash

###############
### DEFINES ###
###############

# the link used
LINKTOCURL="http://www.google.fr"

# the field which is searched
FIELD_TO_SEARCH="field"

# mail title when found
MAIL_TITLE_OK="[Raspi] this is a title"

# mail address
MAIL_ADDRESS=""

# the time the script sleep between two queries in seconds (
SLEEP_TIME_BETWEEN_TWO_QUERIES=0

# logs messages
LOG_MSG_OK="In stock, send mail then exit"
LOG_MSG_KO="Not in stock, script will sleep $SLEEP_TIME_BETWEEN_TWO_QUERIES seconds"

############# CHANGE THE VALUE OF '-eq' IN THE PROGRAM 








###############
###   MAIN  ###
###############

LOGFILE="$0.log"
touch "$LOGFILE"
while [ true ]; do
    RET=`curl -s "$LINKTOCURL" | grep "$FIELD_TO_SEARCH" | wc -c`
    DATE=`date +"%d/%m/%Y %H:%M:%S"`
    if [ "$RET" -eq 0 ]; then
        echo "$LINKTOCURL" | mail -s "$MAIL_TITLE_OK" "$MAIL_ADDRESS"
        echo "[$DATE]    $LOG_MSG_OK" >> "$LOGFILE"
        exit 0
    fi
    echo "[$DATE]    $LOG_MSG_KO" >> "$LOGFILE"
    sleep "$SLEEP_TIME_BETWEEN_TWO_QUERIES"
done
