#!/bin/bash

#to use generics functions and aliases
source ~/.myconf/FUNC_ALIASES_GEN

####################################
#########       VARS       #########
####################################

# header message
HEADER_MSG="####################\nRASPI HAS REBOOTED\n####################"




####################################
#########     FUNCTIONS    #########
####################################

# wait to be sure the pi is ready (internet ready too)
function wait_to_be_sure_pi_is_ready()
{
	sleep 60
};

# get the real ip
function getIp()
{
	CURRENT_IP=`curl --silent http://ipinfo.io/ip`	
};

# send the mail
function send_the_mail()
{
	printf "$HEADER_MSG\n\n[myip]\n"$CURRENT_IP"\n\n[ifconfig]\n`/sbin/ifconfig`\n" | mail -s "[Raspi] Reboot" placeduneufbourg@outlook.fr
};




####################################
#########       MAIN       #########
####################################

wait_to_be_sure_pi_is_ready
getIp
send_the_mail

