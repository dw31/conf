# Download: [VS Code](https://code.visualstudio.com/download)

# Unix User Preference:
```
{
    "workbench.colorTheme": "Monokai",
    "editor.fontSize": 10,
    "workbench.statusBar.feedback.visible": false,
    "workbench.activityBar.visible": true,
    "files.autoSave": "off",
    "terminal.integrated.cursorBlinking": true,
    "terminal.integrated.fontSize": 10,
    "window.restoreWindows": "none",
}
```

# Windows User Preference:
```
{
    "git.ignoreMissingGitWarning": true,
    "workbench.colorTheme": "Monokai",
    "workbench.statusBar.feedback.visible": false,
    "editor.fontSize": 11,
    "workbench.activityBar.visible": true,
    "files.autoSave": "off",
    "terminal.integrated.shell.windows": "C:\\Windows\\System32\\bash.exe",
    "terminal.integrated.cursorBlinking": true,
    "terminal.integrated.fontSize": 10,
    "window.restoreWindows": "none",
}
```

# Plugins:
* Auto Close Tag
* Beautify
* C/C++
* Code Runner
* ESLint
* PHP IntelliSense
* Twig