#!/bin/bash

####################################
#########       VARS       #########
####################################

# script name (ex: 106_lbc)
SCRIPT_NAME=""

# the url used to build db (ex: https://www.leboncoin.fr/voitures/offres/midi_pyrenees/?th=1&q=106&pe=7&me=200000&fu=2)
URLTOPARSE=""

# category url with slash (ex: www.leboncoin.fr/voitures/)
CATEG_URL_WITH_SLASH=""

# value for ca in url (ex: ca=16_s)
CA_EQUAL=""

# mail (1 = part before @, 2 = part after @)
MAIL1=""
MAIL2=""





####################################
#########       CONST      #########
####################################

# db file
DB_FILE="$HOME""/.""$SCRIPT_NAME""_db"

# path where the script work
WORKPATH="$HOME""/.""$SCRIPT_NAME""_tmp"

# tmpfile where the ids are stored
TMP_IDS_FILE="$WORKPATH""/""$SCRIPT_NAME""_TMP_IDS_FILE"

# tmpfile where the links are stored
TMP_LINKS_FILE="$WORKPATH""/""$SCRIPT_NAME""_TMP_LINKS_FILE"




####################################
#########     FUNCTIONS    #########
####################################


function script_start ()
{
  #create db file if not exist
  touch "$DB_FILE"

  #create workpath
  rm -rf "$WORKPATH"
  mkdir -p "$WORKPATH"

  #create tmpfiles
  touch "$TMP_IDS_FILE"
  touch "$TMP_LINKS_FILE"
};

function get_ids_from_link ()
{
  curl --silent "$1" | grep "$CATEG_URL_WITH_SLASH" | cut -d "\"" -f2 | grep "$CA_EQUAL" | cut -d "/" -f 5 | cut -d "." -f1 >> "$TMP_IDS_FILE"
};

function id_2_link ()
{
	while read -r line; do
		if [ ` grep "$line" "$DB_FILE" | wc -l | tr -d ' '` -eq 0 ]; then
			echo "https://""$CATEG_URL_WITH_SLASH""$line.htm""?""$CA_EQUAL" >> "$TMP_LINKS_FILE"
			echo "$line" >> "$DB_FILE"
		fi
	done < "$TMP_IDS_FILE"
};

function send_mail ()
{
	if [ `cat "$TMP_LINKS_FILE" | wc -c | tr -d ' '` -gt 1 ]; then
		MAILCONTENT=`cat "$TMP_LINKS_FILE"`
    echo "$MAILCONTENT" | mail -s "[1303] $SCRIPT_NAME" "$MAIL1@$MAIL2"
	fi
};

function script_end ()
{
	rm -rf "$WORKPATH"
};



####################################
#########       MAIN       #########
####################################

script_start

get_ids_from_link "$URLTOPARSE"

id_2_link

send_mail

script_end
