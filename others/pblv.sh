#!/bin/bash

#to use fmup function
source ~/.myconf/FUNC_ALIASES_GEN

####################################
#########       VARS       #########
####################################

# the root url defined for dl
DL_URL="http://www.planet-series.tv/plus-belle-la-vie/#s12-vf"

#the path where all is done
PATH_ROOT="$HOME/.pblv"

#the log file (all output is redirected to this file)
LOGFILE="$PATH_ROOT/log"

#as a db file, is used to store filenames already downloaded
CACHEFILE="$PATH_ROOT/cache"

#tmpfiles used
TMPFILE_EXTRACTED_LINKS="$PATH_ROOT/extracted_links.tmp"
TMPFILE_CURDL="$PATH_ROOT/curdl.tmp"
TMPFILE_MAIL="$PATH_ROOT/mail.tmp"



####################################
#########     FUNCTIONS    #########
####################################

# write in log
function log_write()
{
	echo "[`date`] $1" >> "$LOGFILE"

	echo "[`date`] $1" >> "$TMPFILE_MAIL"
};

# init what is needed and write the start log msg
function script_start()
{
	mkdir -p "$PATH_ROOT"
	touch "$LOGFILE"
	touch "$CACHEFILE"

	#init mail
		rm -rf "$TMPFILE_MAIL"
		echo "To: placeduneufbourg@outlook.fr" > "$TMPFILE_MAIL"
		echo "From: 31dw31@gmail.com" >> "$TMPFILE_MAIL"
		echo "Subject: [RASPI] Log PBLV" >> "$TMPFILE_MAIL"
	#
	log_write "SCRIPT STARTED"
};

# download the page and extract the last links
function download_page_and_extract_links()
{
	curl --silent "$DL_URL" | grep ">Episode " | tail -63 | head | cut -d "\"" -f24 > "$TMPFILE_EXTRACTED_LINKS"
	log_write "`grep "http://www.planet-series.tv/link-" $TMPFILE_EXTRACTED_LINKS | wc -l` links extracted"
};

# build the filename from the current link
function build_filename()
{
	CUR_FILENAME=`curl --silent "$1" | cut -d "\"" -f2 | awk -F"/" '{print $NF}'`
};

# download the current link if its needed
function download_filename_if_needed()
{
	log_write "name$3 -> $2"
	log_write "link$3 -> $1"

	if [ `grep "$2" "$CACHEFILE" | wc -c` -ne 0 ]; then
		log_write "ret$3  -> already downloaded (found in cache file)"
	else
		TMP_FREE_LINK=`curl --silent "http://www.planet-series.tv/link-gHaszf.html" | cut -d "\"" -f2`
		TMP_FREE_LINK=`fmup $TMP_FREE_LINK | grep "dl.free"`
		if [ "$TMP_FREE_LINK" = "" ]; then
			log_write "ret$3  -> cannot find dl.free link"
		else
			log_write "dl$3   -> start download"
			curl --silent "$TMP_FREE_LINK" -o "$TMPFILE_CURDL"
			if [ "$?" -eq 0 ]; then
				mv "$TMPFILE_CURDL" "$HOME/$2"
				echo "$2" >> "$CACHEFILE"
				log_write "dl$3   -> dl done with success"
			else
				rm -rf "$TMPFILE_CURDL"
				log_write "dl$3   -> dl error"
			fi
		fi
	fi
};

# clean what is needed and write the end log msg
function script_end()
{
	rm -rf "$TMPFILE_EXTRACTED_LINKS"

	log_write "SCRIPT ENDED"

	#send mail and delete the tmp file associated
	/usr/sbin/ssmtp placeduneufbourg@outlook.fr < "$TMPFILE_MAIL"
	rm -rf "$TMPFILE_MAIL"

};




####################################
#########       MAIN       #########
####################################

script_start

download_page_and_extract_links

#for each on extracted links
	COUNTER=1
	while IFS='' read -r line || [[ -n "$line" ]]; do
		build_filename $line
		download_filename_if_needed "$line" "$CUR_FILENAME" "$COUNTER"
		COUNTER=$[$COUNTER +1]
	done < "$TMPFILE_EXTRACTED_LINKS"
#

script_end
