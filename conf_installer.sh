#!/bin/bash

#######################################
#########       DEFINES       #########
#######################################

INSTALL_DIRNAME="$HOME/.myconf"

GIT_DIR_URL="https://gitlab.com/dw31/conf/raw/master"

PBLVSCRIPT="https://gitlab.com/dw31/conf/raw/master/others/pblv.sh"

PIREBOOTSCRIPT="https://gitlab.com/dw31/conf/raw/master/others/pi_reboot.sh"

GENFINDERSCRIPT="https://gitlab.com/dw31/conf/raw/master/others/genFinder.sh"

GENLBCSCRIPT="https://gitlab.com/dw31/conf/raw/master/others/genLbc.sh"

LOYERSCRIPT="https://gitlab.com/dw31/conf/raw/master/others/loyer.sh"



####################################
#########     FUNCTIONS    #########
####################################

function print_header()
{
	echo '----------------------------'
	echo '--    my conf installer   --'
	echo '----------------------------'
	echo ''
};

function check_environment()
{
	printf "Check environment..."
	if [ "$HOME" = "" ]; then
		echo ""
		echo "error: HOME not set"
		exit 1
	fi
	if [ "$MYCFGSYS" = "" ]; then
		echo ""
		echo "error: MYCFGSYS not set"
		echo "export MYCFGSYS [home/pro/vps]"
		exit 1
	fi
	if [ "$MYCFGSYS" != "home" ] && [ "$MYCFGSYS" != "pro" ] && [ "$MYCFGSYS" != "vps" ]; then
		echo ""
		echo "error: bad value for MYCFGSYS"
		echo "export MYCFGSYS [home/pro/vps]"
		exit 1
	fi
	echo " DONE "
	printf "Detect system..."
	echo "     DONE"
};

function clone_myconf_dir()
{
	printf "DL conf files..."
	rm -rf "$INSTALL_DIRNAME"
	mkdir "$INSTALL_DIRNAME"
	cd "$INSTALL_DIRNAME" 1>/dev/null
	curl --silent -O "$GIT_DIR_URL/modules/CONF_HANDLER"
	curl --silent -O "$GIT_DIR_URL/modules/PATH_EXPORT_GEN"
	curl --silent -O "$GIT_DIR_URL/modules/FUNC_ALIASES_GEN"
	curl --silent -O "$GIT_DIR_URL/modules/PATH_EXPORT_HOME"
	curl --silent -O "$GIT_DIR_URL/modules/FUNC_ALIASES_HOME"
	curl --silent -O "$GIT_DIR_URL/modules/PATH_EXPORT_PRO"
	curl --silent -O "$GIT_DIR_URL/modules/FUNC_ALIASES_PRO"
	curl --silent -O "$GIT_DIR_URL/modules/PATH_EXPORT_VPS"
	curl --silent -O "$GIT_DIR_URL/modules/FUNC_ALIASES_VPS"
	cd - 1>/dev/null
	echo "     DONE "
};

function add_link_in_dotrc()
{
	printf "Set bashrc..."
	echo "export MYCFGSYS=$MYCFGSYS" > $HOME/.bash_profile
	echo "source $INSTALL_DIRNAME/CONF_HANDLER" >> $HOME/.bash_profile
	ln -s $HOME/.bash_profile $HOME/.bashrc
	echo "         DONE "
};

function add_scripts()
{
	printf "Add scripts..."
	if [ "$MYCFGSYS" = "vps" ] ; then
		rm -rf $HOME/.script 
		mkdir -p $HOME/.script
		
		# pi_reboot script
			curl --silent -O "$PIREBOOTSCRIPT"
			mv pi_reboot.sh $HOME/.script
			chmod +x $HOME/.script/pi_reboot.sh
		#
		
		# genFinder script
			curl --silent -O "$GENFINDERSCRIPT"
			mv genFinder.sh $HOME/.script
			chmod +x $HOME/.script/genFinder.sh
		#
		
		# genLbc script
			curl --silent -O "$GENLBCSCRIPT"
			mv genLbc.sh $HOME/.script
			chmod +x $HOME/.script/genLbc.sh

    # loyer script
			curl --silent -O "$LOYERSCRIPT"
			mv loyer.sh $HOME/.script
			chmod +x $HOME/.script/loyer.sh
		#
		
		echo "       DONE"
	else
		echo "       NOT DONE (no need)"
	fi
};

function add_vim_conf()
{
	printf "Add vim conf..."
	rm -rf $HOME/.vim
	mkdir -p $HOME/.vim
	curl --silent -O "$GIT_DIR_URL/others/vim.tar"
	tar xf vim.tar -C $HOME/.vim
	mv $HOME/.vim/vimrc $HOME/.vimrc
	rm -f vim.tar
	echo "      DONE"
};

function display_source_msg()
{
	echo ""
	echo "Use this command to apply the changes:"
	echo "source $HOME/.bash_profile"
};




####################################
#########       MAIN       #########
####################################

print_header
check_environment
clone_myconf_dir
add_link_in_dotrc
add_scripts
add_vim_conf
display_source_msg
